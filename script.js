let mainClick = document.getElementById("main-click")
let mainCounter = document.getElementById("main-counter")
let buttonOptions = document.getElementById("button-options")
let buttonInfo = document.getElementById("button-info")
let containerPw = document.getElementById("container-pw")
let clickedCount = document.getElementById("clicks")
let pwCount = document.getElementById("pw-count")
let debuggerButton = document.getElementById("debugger-button")
let closeInfoButton = document.getElementById("close-info") 
let closeOptionsButton = document.getElementById("close-options")

let optionsValue = document.getElementById("container-options")
let infoValue = document.getElementById("container-info")
let valuePw1 = document.getElementById("valor-pw-1")
let valuePw2 = document.getElementById("valor-pw-2")
let valuePw3 = document.getElementById("valor-pw-3")
let valuePw4 = document.getElementById("valor-pw-4")
let valuePw5 = document.getElementById("valor-pw-5")
let valuePw6 = document.getElementById("valor-pw-6")
let valuePw7 = document.getElementById("valor-pw-7")
let valuePw8 = document.getElementById("valor-pw-8")

let pw1 = document.getElementById("pw-1")
let pw2 = document.getElementById("pw-2")
let pw3 = document.getElementById("pw-3")
let pw4 = document.getElementById("pw-4")
let pw5 = document.getElementById("pw-5")
let pw6 = document.getElementById("pw-6")
let pw7 = document.getElementById("pw-7")
let pw8 = document.getElementById("pw-8")

let pw1counter = document.getElementById("pw1Count")
let pw2counter = document.getElementById("pw2Count")
let pw3counter = document.getElementById("pw3Count")
let pw4counter = document.getElementById("pw4Count")
let pw5counter = document.getElementById("pw5Count")
let pw6counter = document.getElementById("pw6Count")
let pw7counter = document.getElementById("pw7Count")
let pw8counter = document.getElementById("pw8Count")

let pw1Icon = document.getElementById("container-pw-1")
let pw2Icon = document.getElementById("container-pw-2")
let pw3Icon = document.getElementById("container-pw-3")
let pw4Icon = document.getElementById("container-pw-4")
let pw5Icon = document.getElementById("container-pw-5")
let pw6Icon = document.getElementById("container-pw-6")
let pw7Icon = document.getElementById("container-pw-7")
let pw8Icon = document.getElementById("container-pw-8")

pw1Icon.setAttribute("class", "hidden")
pw2Icon.setAttribute("class", "hidden")
pw3Icon.setAttribute("class", "hidden")
pw4Icon.setAttribute("class", "hidden")
pw5Icon.setAttribute("class", "hidden")
pw6Icon.setAttribute("class", "hidden")
pw7Icon.setAttribute("class", "hidden")
pw8Icon.setAttribute("class", "hidden")

let pw1Count = 0
let pw2Count = 0
let pw3Count = 0
let pw4Count = 0
let pw5Count = 0
let pw6Count = 0
let pw7Count = 0
let pw8Count = 0

let optionsCounter = 0
let infoCounter = 0
let counter = 0
let pwInfoCount = 0
let clicked = 0
let clickMouse = 1
let pw1Price = 10
let pw2Price = 1200
let pw3Price = 10000
let pw4Price = 45000
let pw5Price = 200000
let pw6Price = 1000000
let pw7Price = 5000000
let pw8Price = 25000000

let delay=1000;

setInterval(() => {

    canBuyPw1()
    canBuyPw2()
    canBuyPw3()
    canBuyPw4()
    canBuyPw5()
    canBuyPw6()
    canBuyPw7()
    canBuyPw8()

    mainCounter.innerHTML = Math.trunc(counter)
    clickedCount.innerText = "clicks = " + clicked
    pwCount.innerText = "PowerUps comprados = " + pwInfoCount
    pw1counter.innerHTML = "comprado: " + pw1Count
    pw2counter.innerHTML = "comprado: " + pw2Count
    pw3counter.innerHTML = "comprado: " + pw3Count
    pw4counter.innerHTML = "comprado: " + pw4Count
    pw5counter.innerHTML = "comprado: " + pw5Count
    pw6counter.innerHTML = "comprado: " + pw6Count
    pw7counter.innerHTML = "comprado: " + pw7Count
    pw8counter.innerHTML = "comprado: " + pw8Count
    valuePw1.innerHTML = Math.trunc(pw1Price)
    valuePw2.innerHTML = Math.trunc(pw2Price)
    valuePw3.innerHTML = Math.trunc(pw3Price)
    valuePw4.innerHTML = Math.trunc(pw4Price)
    valuePw5.innerHTML = Math.trunc(pw5Price)
    valuePw6.innerHTML = Math.trunc(pw6Price)
    valuePw7.innerHTML = Math.trunc(pw7Price)
    valuePw8.innerHTML = Math.trunc(pw8Price)
}, 10)

function counterClick(){
    counter += clickMouse
    clicked++
}

function powerUp1(){
    pwInfoCount++
    pw1Count++
    clickMouse += 0.2
    counter = counter - pw1Price 
    pw1Price = Math.trunc(pw1Price + (pw1Price / 4))
    for(let i = 0; i < 1; i++){
        setInterval(() => {
            counter++
        }, delay)
    }
    containerIconHidden1(pw1Count)
    makePw1Img()
}
function powerUp2(){
    pwInfoCount++
    pw2Count++
    clickMouse += 0.5
    counter = counter - pw2Price 
    pw2Price = Math.trunc(pw2Price + (pw2Price / 3.6))
    for(let i = 0; i < 1; i++){
        setInterval(() => {
            counter += 40
        }, delay)
    }
    makePw2Img()
    containerIconHidden2(pw2Count)
}
function powerUp3(){
    pwInfoCount++
    pw3Count++
    clickMouse += 0.9
    counter = counter - pw3Price 
    pw3Price = Math.trunc(pw3Price + (pw3Price / 3.5))
    for(let i = 0; i < 1; i++){
        setInterval(() => {
            counter += 80
        }, delay)
    }
    makePw3Img()
    containerIconHidden3(pw3Count)
}
function powerUp4(){
    pwInfoCount++
    pw4Count++
    clickMouse += 1.1
    counter = counter - pw4Price 
    pw4Price = Math.trunc(pw4Price + (pw4Price / 3.4))
    for(let i = 0; i < 1; i++){
        setInterval(() => {
            counter += 140
        }, delay)
    }
    makePw4Img()
    containerIconHidden4(pw4Count)
}
function powerUp5(){
    pwInfoCount++
    pw5Count++
    clickMouse += 1.5
    counter = counter - pw5Price 
    pw5Price = Math.trunc(pw5Price + (pw5Price / 3.4))
    for(let i = 0; i < 1; i++){
        setInterval(() => {
            counter += 300
        }, delay)
    }
    makePw5Img()
    containerIconHidden5(pw5Count)
}
function powerUp6(){
    pwInfoCount++
    pw6Count++
    clickMouse += 2.0
    counter = counter - pw6Price 
    pw6Price = Math.trunc(pw6Price + (pw6Price / 3.2))
    for(let i = 0; i < 1; i++){
        setInterval(() => {
            counter += 700
        }, delay)
    }
    makePw6Img()
    containerIconHidden6(pw6Count)
}
function powerUp7(){
    pwInfoCount++
    pw7Count++
    clickMouse += 2.5
    counter = counter - pw7Price 
    pw7Price = Math.trunc(pw7Price + (pw7Price / 3.1))
    for(let i = 0; i < 1; i++){
        setInterval(() => {
            counter += 1500
        }, delay)
    }
    makePw7Img()
    containerIconHidden7(pw7Count)
}
function powerUp8(){
    pwInfoCount++
    pw8Count++
    clickMouse += 2.5
    counter = counter - pw8Price 
    pw8Price = Math.trunc(pw8Price + (pw8Price / 3.0))
    for(let i = 0; i < 1; i++){
        setInterval(() => {
            counter += 1500
        }, delay)
    }
    makePw8Img()
    containerIconHidden8(pw8Count)
}

function makePw1Img(){
    if(pw1Icon.childElementCount < 22){
        let newImagepw1 = document.createElement("img")
        newImagepw1.setAttribute("id", "newImagepw1")
        newImagepw1.style.width = "40px"
        newImagepw1.src = "img/inseguro.png"
        pw1Icon.appendChild(newImagepw1)
    }
}
function makePw2Img(){
    if(pw2Icon.childElementCount < 22){
        let newImagepw2 = document.createElement("img")
        newImagepw2.setAttribute("id", "newImagepw2")
        newImagepw2.style.width = "40px"
        newImagepw2.src = "img/fbicon2.png"
        pw2Icon.appendChild(newImagepw2)
    }
}
function makePw3Img(){
    if(pw3Icon.childElementCount < 22){
        let newImagepw3 = document.createElement("img")
        newImagepw3.setAttribute("id", "newImagepw3")
        newImagepw3.style.width = "40px"
        newImagepw3.src = "img/cellAndroid.png"
        pw3Icon.appendChild(newImagepw3)
    }
}
function makePw4Img(){
    if(pw4Icon.childElementCount < 22){
        let newImagepw4 = document.createElement("img")
        newImagepw4.setAttribute("id", "newImagepw4")
        newImagepw4.style.width = "40px"
        newImagepw4.src = "img/detranicon.png"
        pw4Icon.appendChild(newImagepw4)
    }
}
function makePw5Img(){
    if(pw5Icon.childElementCount < 22){
        let newImagepw5 = document.createElement("img")
        newImagepw5.setAttribute("id", "newImagepw5")
        newImagepw5.style.width = "40px"
        newImagepw5.src = "img/bancoicon.png"
        pw5Icon.appendChild(newImagepw5)
    }
}
function makePw6Img(){
    if(pw6Icon.childElementCount < 22){
        let newImagepw6 = document.createElement("img")
        newImagepw6.setAttribute("id", "newImagepw6")
        newImagepw6.style.width = "40px"
        newImagepw6.src = "img/brasillogo.png"
        pw6Icon.appendChild(newImagepw6)
    }
}
function makePw7Img(){
    if(pw7Icon.childElementCount < 22){
        let newImagepw7 = document.createElement("img")
        newImagepw7.setAttribute("id", "newImagepw7")
        newImagepw7.style.width = "40px"
        newImagepw7.src = "img/ABIN.jpg"
        pw7Icon.appendChild(newImagepw7)
    }
}
function makePw8Img(){
    if(pw8Icon.childElementCount < 22){
        let newImagepw8 = document.createElement("img")
        newImagepw8.setAttribute("id", "newImagepw8")
        newImagepw8.style.width = "40px"
        newImagepw8.src = "img/trump.png"
        pw8Icon.appendChild(newImagepw8)
    }
}
function canBuyPw1(){
    if(counter >= pw1Price){
        pw1.addEventListener("click", powerUp1)
        pw1.removeAttribute("class", "blur")
    }
    else{
        pw1.removeEventListener("click", powerUp1)
        pw1.setAttribute("class", "")
        pw1.className += 'blur'
    }
}
function canBuyPw2(){
    if(counter >= pw2Price){
        pw2.addEventListener("click", powerUp2)
        pw2.removeAttribute("class", "blur")
    }
    else{
        pw2.removeEventListener("click", powerUp2)
        pw2.setAttribute("class", "")
        pw2.className += 'blur'
    }
}
function canBuyPw3(){
    if(counter >= pw3Price){
        pw3.addEventListener("click", powerUp3)
        pw3.removeAttribute("class", "blur")
    }
    else{
        pw3.removeEventListener("click", powerUp3)
        pw3.setAttribute("class", "blur")
    }
}
function canBuyPw4(){
    if(counter >= pw4Price){
        pw4.addEventListener("click", powerUp4)
        pw4.removeAttribute("class", "blur")
    }
    else{
        pw4.removeEventListener("click", powerUp4)
        pw4.setAttribute("class", "blur")
    }
}
function canBuyPw5(){
    if(counter >= pw5Price){
        pw5.addEventListener("click", powerUp5)
        pw5.removeAttribute("class", "blur")
    }
    else{
        pw5.removeEventListener("click", powerUp5)
        pw5.setAttribute("class", "blur")
    }
}
function canBuyPw6(){
    if(counter >= pw6Price){
        pw6.addEventListener("click", powerUp6)
        pw6.removeAttribute("class", "blur")
    }
    else{
        pw6.removeEventListener("click", powerUp6)
        pw6.setAttribute("class", "blur")
    }
}
function canBuyPw7(){
    if(counter >= pw7Price){
        pw7.addEventListener("click", powerUp7)
        pw7.removeAttribute("class", "blur")
    }
    else{
        pw7.removeEventListener("click", powerUp7)
        pw7.setAttribute("class", "blur")
    }
}
function canBuyPw8(){
    if(counter >= pw8Price){
        pw8.addEventListener("click", powerUp8)
        pw8.removeAttribute("class", "blur")
    }
    else{
        pw8.removeEventListener("click", powerUp8)
        pw8.setAttribute("class", "blur")
    }
}

function containerIconHidden1(){
    pw1Icon.removeAttribute("class", "hidden")
}
function containerIconHidden2(){
    pw2Icon.removeAttribute("class", "hidden")
}
function containerIconHidden3(){
    pw3Icon.removeAttribute("class", "hidden")
}
function containerIconHidden4(){
    pw4Icon.removeAttribute("class", "hidden")
}
function containerIconHidden5(){
    pw5Icon.removeAttribute("class", "hidden")
}
function containerIconHidden6(){
    pw6Icon.removeAttribute("class", "hidden")
}
function containerIconHidden7(){
    pw7Icon.removeAttribute("class", "hidden")
}
function containerIconHidden8(){
    pw8Icon.removeAttribute("class", "hidden")
}

function showOptions(){
    optionsCounter = 1
    if(optionsCounter === 1 && infoCounter === 0){
        optionsValue.removeAttribute("class", "hidden")
        containerPw.setAttribute("class", "hidden")
    }
}
function showInfo(){
    infoCounter = 1
    if(infoCounter === 1 && optionsCounter === 0){
        infoValue.removeAttribute("class", "hidden")
        containerPw.setAttribute("class", "hidden")
    }
}
function closeInfo(){
    infoCounter = 0
    infoValue.setAttribute("class", "hidden")
    containerPw.removeAttribute("class", "hidden")
}
function closeOptions(){
    optionsCounter = 0
    optionsValue.setAttribute("class", "hidden")
    containerPw.removeAttribute("class", "hidden")
}

mainClick.addEventListener("click", counterClick)
buttonOptions.addEventListener("click", showOptions)
buttonInfo.addEventListener("click", showInfo)
debuggerButton.addEventListener("click", debbugMoney)
closeInfoButton.addEventListener("click", closeInfo)
closeOptionsButton.addEventListener("click", closeOptions)



console.log("Oque faz aqui amigao?")
console.log("Se quiser so testar a PowerUp da um counter = 10000000000000 ae")

function debbugMoney(){
    counter += 100000000000000000000000000000000000
}